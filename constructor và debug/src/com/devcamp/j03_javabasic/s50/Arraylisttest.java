package com.devcamp.j03_javabasic.s50;

import java.util.ArrayList;
import java.util.Arrays;

public class Arraylisttest {
    public static void main(String[] args) {
        Integer[] arraylist = new Integer[100];

        for(int i = 1; i < 100 ; i++){
            arraylist[i] = i;
        }
        for(int i =0 ; i < arraylist.length ; i ++){
            // System.out.println(arraylist[i]);
        }
        // convert từ array sang array list
        ArrayList<Integer> arrayList2 = new ArrayList<>(Arrays.asList(arraylist));
        // System.out.println(arrayList2);

        ArrayList<Integer> arrayList3 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            arrayList3.add(i);

            // System.out.println(i);
        }
        for (int j : arrayList3) {
            // System.out.print(" "+j);
            }

            // convert từ arraylist sang array
        Integer[] results = arrayList3.stream().toArray(size -> new Integer[size]);
        for(Integer index : results){
        // System.out.println(index);
        }

        ArrayList<Integer> arrayList4 = new ArrayList<>();
        ArrayList<Object> arrayList5 = new ArrayList<>();
        
        arrayList4.add(12);
        arrayList4.add(2);
        arrayList4.add(4);
        arrayList4.add(6);
        arrayList4.add(8);
        System.out.println(arrayList4);
        // lấy vị trị index
        arrayList4.lastIndexOf(8);
        System.out.println(arrayList4.lastIndexOf(8));
        // thêm giá trị vào vị trí index
        arrayList4.add(1, 5);
        System.out.println(arrayList4);
        // xóa một object trong arraylist
        arrayList4.remove(0);
        System.out.println(arrayList4);
        //Cập nhật phần tử tại vị trí index.
        arrayList4.set(3, 30);
        System.out.println(arrayList4);
        // Lấy vị trí index của object o trong ArrayList
        arrayList4.indexOf(3);
        System.out.println(arrayList4);
        Object a = arrayList4.get(4);
        System.out.println(a);
        int b = arrayList4.size();
        System.out.println(b);

        boolean h = arrayList4.contains(4);
        System.out.println(h);

        arrayList4.clear();
        System.out.println(arrayList4);
        
            
        
    }

}
