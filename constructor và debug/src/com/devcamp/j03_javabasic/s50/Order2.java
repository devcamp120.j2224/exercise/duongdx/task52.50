package com.devcamp.j03_javabasic.s50;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
// Import  thư viện
import java.util.Locale;


public class Order2 {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate = new Date(); // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    Person buyer = new Person("duong", 29);
    // khởi tạo một tham số customerName
    public Order2(String customerName){
        this.id = 1 ;
        this.customerName = customerName ;
        this.price = 120000 ;
        this.orderDate = new Date();
        this.confirm = true ;
        this.items = new String[]{"book", "pen", "rule"};
    }
    // khởi tạo với tất cả tham số
    public Order2(int id , String customerName, long price, Date orderDate, boolean confirm, String[] items, Person person){
        this.id = id ;
        this.customerName = customerName ;
        this.price = price ;
        this.orderDate = orderDate ;
        this.confirm = true ;
        this.items = new String[] {"book", "pen", "rule"};
        this.buyer = person;
        // System.out.println(buyer);
    }
    // khởi tạo không tham số
    public Order2(){
        this("2");
    }

    //khởi tạo với 3 tham số
    public Order2(int id, String customerName, long price){
        this.id = id ;
        this.customerName = customerName ;
        this.price = price ;
    }

    @Override
    public String toString(){
        // định dạng tiêu chuẩn việt nam
        Locale.setDefault(new Locale("US" , "US"));
        // định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usnNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return trả ra chuỗi String
        return 
            "Order [id =" + id 
            + ", customerName =" + customerName
            + ", Price =" + usnNumberFormat.format(price)
            + ", orderDate =" + dateTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm :" + confirm
            + ", items = " + Arrays.toString(items)
            + ",age= " + buyer.name + buyer.age ;
    }
}
