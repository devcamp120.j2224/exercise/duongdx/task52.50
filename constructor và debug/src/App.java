import java.util.ArrayList;
import java.util.Date;
// import com.devcamp.j03_javabasic.s50.Order;
import com.devcamp.j03_javabasic.s50.Order2;
import com.devcamp.j03_javabasic.s50.Person;

public class App {
    public static void main(String[] args) throws Exception {
       
        


        // ArrayList<Long> arrayListLong = new ArrayList<Long>() ;
        // for (int i=0; i<100;i++){
        //     arrayListLong.add((long) i);
        // }
        // for(Long  nguoi : arrayListLong){
        //     System.out.println(nguoi.toString());
        // }
        // ArrayList<Order> arrayList = new ArrayList<>() ;
        ArrayList<Order2> arrayList2 = new ArrayList<>() ;
        // Khởi tạo một hàm với các tham số khác nhau ( khởi tạo 4 object order)
        Person person1 = new Person("dahfewfew", 20);
        // Order order1 = new Order();
        // Order order2 = new Order("duong");
        // Order order3 = new Order(3 , "dung" , 80000);
        // Order order4 = new Order(5, "nam" , 75000 , new Date() , false , new String[]{"phan", "but", "tay"});
        // Khởi tạo 4 đối tượng của order2
        Order2 nguoi1 = new Order2("Nguyen Van An");
        // Order2 nguoi2 = new Order2(13 , "Dao Xuan Duong");
        Order2 nguoi3 = new Order2(14 , "Thuy Linh", 30000);
        Order2 nguoi4 = new Order2(15,"Dao van Thuan",122000 ,new Date(),true, new String[]{"Coffee", "tea" ,"milk"},person1);
        // thêm object order vào danh sách
        // arrayList.add(order1);
        // arrayList.add(order2);
        // arrayList.add(order3);
        // arrayList.add(order4);
        arrayList2.add(nguoi1);
        // arrayList2.add(nguoi2);
        arrayList2.add(nguoi3);
        arrayList2.add(nguoi4);
        // for(Order order : arrayList){
        //     // System.out.println(order.toString());
        // }
        for(Order2 nguoi : arrayList2){
            System.out.println(nguoi.toString());
        }
    }
}
